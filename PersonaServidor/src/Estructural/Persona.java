package Estructural;

import java.text.SimpleDateFormat;

import java.util.Date;

public class Persona {
    
    private int id;
    
    private String nombre;
    
    private String profesion;
    
    private Date fechaNacimiento;    
      
    public Persona(){
        this.id = 0;
        this.nombre = "";
        this.profesion = "";
        this.fechaNacimiento = new Date();
    }
    
    public Persona(int id, String nombre, String profesion, Date fn){
        this.id = id;
        this.nombre = nombre;
        this.profesion = profesion;
        this.fechaNacimiento = fn;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getProfesion() {
        return profesion;
    }    
}
