package Model;

import Conexion.ConexionBD;

import Estructural.Persona;

import java.sql.ResultSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.SimpleTimeZone;

import oracle.jdbc.pool.OracleDataSource;

public class ServicioPersona {
    
    private static List <Persona> personas = new ArrayList<Persona>();
    
    public ServicioPersona() {
        super();
    }
    
    public static boolean crearPersona(Persona p){
        Persona persona = buscarPersona(p.getId());
        if(persona!= null){
            return false;
        }else{
            personas.add(p);
            addPersonaBD(p);
            return true;
        }
    }
    
    public static boolean eliminarPersona(int id){
        Persona persona = buscarPersona(id);
        if(persona != null){
            personas.remove(persona);
            eliminarPersonaBD(id);
            return true;
        }else{
           return false;
        }
    }
    
    public static Persona buscarPersona(int id){
        for(Persona persona: personas){
            if(persona.getId() == id){
                return persona;
            }
        }
        Persona p = buscarPersonaBD(id);
        if(p != null){
            return p;
        }
        return null;
    }
    
    public static boolean modificarPersona(int id, Persona nPersona){
       Persona p = buscarPersona(id);
       if(p!= null){
           eliminarPersona(id);
           crearPersona(nPersona);
           return true;
       }else{
           return true;
       }
    }

    public static void setPersonas(ArrayList<Persona> personas) {
        ServicioPersona.personas = personas;
    }

    public static ArrayList<Persona> getPersonas() {
        personas = new ArrayList<Persona>();
        ListarPersonasBD();
        return (ArrayList<Persona>)personas;        
    }
    
    public static void addPersonaBD(Persona p){
        ConexionBD con = new ConexionBD();
        SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "INSERT INTO PERSONA VALUES("+ p.getId() +", '"+ p.getNombre()+ "', '"+ p.getProfesion()+ "', TO_DATE('"+ d.format(p.getFechaNacimiento()) + "', 'yyyy-MM-dd'))";
        System.out.println(sql);
        con.executeUpdateStatement(sql);
    }
    
    public static void ListarPersonasBD(){
        ConexionBD con = new ConexionBD();
        String sql = "select * from persona";
        ResultSet rs = con.executeQueryStatement(sql);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        try{
            while(rs.next()){
                int id = rs.getInt(1);
                String nombre = rs.getString(2);
                String profesion = rs.getString(3);
                String sfecha = rs.getString(4);
                Date fecha = format.parse(sfecha);
                Persona p = new Persona (id, nombre, profesion, fecha);
                personas.add(p);
            }
        }catch(Exception e){
            System.out.println("NO Entre a Consultar los datos");
        }
    }
    
    public static Persona buscarPersonaBD(int id){
        ConexionBD con = new ConexionBD();
        String sql = "select * from persona where(id = " + id +")";
        ResultSet rs = con.executeQueryStatement(sql);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        try{
            while(rs.next()){
                int pid = rs.getInt(1);
                String pNombre = rs.getString(2);
                String pProfesion = rs.getString(3);
                String fecha = rs.getString(4);
                Date pFecha = format.parse(fecha);
                Persona p = new Persona (pid, pNombre, pProfesion, pFecha);
                personas.add(p);
                return p;
            }
        }catch(Exception e){
            System.out.println("Fallo la conexion a la BD");
            return null;
        }
        return null;            
    }
    
    public static void eliminarPersonaBD(int id){
        String sql = "delete from persona where (id = " + id + ")";
        ConexionBD con = new ConexionBD();
        boolean elimino = con.executeUpdateStatement(sql);
        if(elimino){
            System.out.println("Si elimino de la base de datos");
        }
    }
}
