package ModelSW;

import Estructural.Persona;

import java.util.Date;

import Model.ServicioPersona;

import java.util.ArrayList;

import javax.jws.WebService;

import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;

@WebService(name = "SWPersona2", serviceName = "SWPersona", targetNamespace = "http://tempuri.org/")
public class SWPersona {
    
    public SWPersona() {
        super();
    }
    
    public boolean agregarPersona(Persona p){
       return ServicioPersona.crearPersona(p);
    }
    
    public boolean eliminarPersona(int id){
        return ServicioPersona.eliminarPersona(id);
    }
    public ArrayList <Persona> listarPersonas(){
        return ServicioPersona.getPersonas();
    }
    
    public Persona buscarPersona(int id){
        return ServicioPersona.buscarPersona(id);
    }
    
    public boolean ModificarPersona(int id, Persona p){
        return ServicioPersona.modificarPersona(id, p);
    }
}
